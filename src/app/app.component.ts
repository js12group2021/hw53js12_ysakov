import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'To Do';
  thingsToDo = [
    'Buy milk',
    'Walk with dog',
    'Do homework'
  ];

  onNewItem(item: string) {
    this.thingsToDo.push(item);
  }
}
